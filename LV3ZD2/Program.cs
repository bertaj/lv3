using System;

namespace LV3Z2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrix= new double[3][];
            for (int i = 0; i < 3; i++)
            {
                matrix[i] = new double[3];
            }
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            matrix = matrixGenerator.NextMatrix(3, 3);
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(matrix[i][j].ToString() + '\t');
                }
                Console.WriteLine();
            }

        }
    }
}
