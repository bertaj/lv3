using System;
using System.Collections.Generic;
using System.Text;

namespace LV3Z1
{
    interface Prototype
    {
        Prototype Clone();
    }
}
